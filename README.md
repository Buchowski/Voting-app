## Simple Vote-app
This is a simple voting app


# Run application using Docker


## Prerequisites:
* Docker (https://docs.docker.com/engine/installation/)

1. Pull this repo, go to project directory and create docker image:
  
    ```    
    docker build -t voteapp .

    ```
2. Create and run Docker container with created image 
    (make sure you have ports 9000, 3000, 35729 available)
    
    ```    
    docker run --restart=always -d -p 9000:9000 -p 3000:3000 -p 35729:35729 --name voteAppContainer voteapp

    ```
3.  run application on http://localhost:9000/


# or locally :

## Prerequisites:
* mongodb
* node
* compass
* bower
* grunt

1. Pull this repo, go to server directory, install express and run server:
  
    ```    
    npm install express
    node index.js

    ```
    From now your server is connected to mongodb and listening on port 3000
    
2. Go to client directory install all dependencies and run client:
    
    ```    
    npm install
    bower install
    grunt serve

    ```
3.  run application on http://localhost:9000/
