'use strict';

/**
 * @ngdoc service
 * @name votingAppApp.mainService
 * @description
 * # mainService
 * Service in the votingAppApp.
 */
angular.module('votingAppApp')
  .service('mainService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
