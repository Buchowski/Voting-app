'use strict';

/**
 * @ngdoc service
 * @name clientApp.mainService
 * @description
 * # mainService
 * Service in the clientApp.
 */
angular.module('clientApp')
  .factory('ServerDataRestangular', ['Restangular', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setRestangularFields({
        id: '_id'
      });
    });
  }])


  .factory('ServerData', ['ServerDataRestangular', function (ServerDataRestangular) {
    return ServerDataRestangular.service('/')
  }])

  .service('ServerRequest', ['ServerData', function (ServerData) {
    this.sendRequest = function (responseCallback) {
      ServerData.getList().then(responseCallback, function (error) {
        console.log("Error with status code", error.status);
      });
    }
  }])

  .service('NewVote', ['ServerRequest', 'InitialValues', function (ServerRequest, InitialValues) {
    this.send = function (vote, reloadData) {
      ServerRequest.sendRequest(function (response) {
        if (response.length > 0) {
          for (var i = 0; i < response.length; i++) {
            if (response[i].title === vote) {
              var object = response[i];
              object.quantity += 1;
              object.put().then(function () {
                reloadData();
              });
            }
          }
        } else {
          InitialValues(vote, reloadData);
        }
      })
    }
  }])


  .factory('InitialValues', ['ServerData', function (ServerData) {
    return function (vote, thenCallback) {
      var postData = {};
      if (vote == "Webstorm") {
        postData = {title: "Webstorm", quantity: 1};
        ServerData.post(postData).then(function () {
          postData = {title: "Netbeans", quantity: 0};
          ServerData.post(postData).then(function () {
            thenCallback();
          })
        })
      } else {
        postData = {title: "Webstorm", quantity: 0};
        ServerData.post(postData).then(function () {
          postData = {title: "Netbeans", quantity: 1};
          ServerData.post(postData).then(function () {
            thenCallback();
          })
        })
      }
    }
  }]);


