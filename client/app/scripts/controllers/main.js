'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('MainCtrl', ['$scope', 'ServerData', 'InitialValues', 'ServerRequest', 'NewVote',
    function ($scope, ServerData, InitialValues, ServerRequest, NewVote) {

      $scope.sendVote = function (vote) {
        NewVote.send(vote, loadData);
        $scope.formData.ideName = '';
      };

      function loadData() {
        $scope.labels = [];
        $scope.data = [[]];
        ServerData.getList().then(function (response) {
          for (var i = 0; i < response.length; i++) {
            var object = response[i];
            $scope.labels.push(object.title);
            $scope.data[0].push(object.quantity);
          }
        })
      }

      loadData();

    }]);
