FROM ubuntu:14.04


WORKDIR /home/votingapp
RUN pwd

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
    nodejs-legacy \
    npm \
    git \
    supervisor \
    ruby-dev \
    make \
    mongodb

RUN gem install --no-rdoc --no-ri compass
RUN npm install -g grunt-cli
RUN npm install -g bower

ADD . /home/votingapp

WORKDIR /home/votingapp/client

RUN npm install
RUN bower install --allow-root

WORKDIR /home/votingapp/server

RUN npm install express


RUN mkdir -p /var/log/supervisor
RUN apt-get update
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE  9000 3000 35729
CMD ["/usr/bin/supervisord"]
