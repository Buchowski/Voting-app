/**
 * Created by rafal on 27.01.16.
 */

var mongoose = require('mongoose');

var VoteSchema = new mongoose.Schema({
    title : {
        type: String,
        required: true
    },
    quantity : {
        type: Number,
        default: 0
    }
});

module.exports = VoteSchema;