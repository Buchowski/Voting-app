/**
 * Created by rafal on 27.01.16.
 */
var restful = require('node-restful');

module.exports = function(app,route) {

    var rest = restful.model(
        'votes',
        app.models.vote
    ).methods(['get','put','post','delete']);

    rest.register(app,route);

    return function(req, res, next) {
      next();
    };

};